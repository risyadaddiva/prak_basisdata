CREATE TABLE `traveloka` (
  `kode` integer PRIMARY KEY,
  `kota_tujuan` varchar(255),
  `id_penumpang` integer,
  `nama_penumpang` varchar(255)
);

CREATE TABLE `tujuan` (
  `kode` integer PRIMARY KEY,
  `id_travel` integer,
  `nama_travel` varchar(255),
  `jam_berangkat` integer,
  `jam_tiba` integer
);

CREATE TABLE `travel` (
  `no_travel` integer PRIMARY KEY,
  `nama_supir` varchar(255),
  `no_supir` integer,
  `harga` integer,
  `warna` varchar(255)
);

ALTER TABLE `tujuan` ADD FOREIGN KEY (`id_travel`) REFERENCES `travel` (`no_travel`);

ALTER TABLE `traveloka` ADD FOREIGN KEY (`kode`) REFERENCES `tujuan` (`kode`);
